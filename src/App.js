import './App.css';
import AllProducts from './component/AllProducts';
import EachProduct from './component/EachProduct';
import NavBar from './component/NavBar';
import { Routes, Route } from 'react-router-dom';

function App() {
  return (
    <>
      <NavBar />
      <Routes>
        <Route path='/' element={<AllProducts />}> </Route>
        <Route path='EachProduct/:id' element={<EachProduct />}></Route>
      </Routes>
    </>
  );
}

export default App;
