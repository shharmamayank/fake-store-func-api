import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'


function EachProduct() {
    const { id } = useParams()
    const API_STATES = {
        Loading: "Loading",
        Loaded: "Loaded",
        Error: "error"
    }
    const [post, SetPost] = useState([])
    const [status, SetStatus] = useState(API_STATES)
    const [IsError, setIsError] = useState("");

    const url = `https://fakestoreapi.com/products/${id}`

    const fetchData = ((url) => {
        SetStatus(API_STATES.Loading)

        fetch(url).then((response) => {
            return response.json()
        }).then((response) => {
            SetPost(response)
            SetStatus(API_STATES.Loaded)
        }).catch((error) => {
            setIsError(
                "An API error occurred while fetching"
            );
            SetStatus(API_STATES.Error);

        })

    })

    useEffect(() => {
        fetchData(url)
    }, [])

    let DataofProducts = post

    console.log(DataofProducts)
    return (
        <>
            {status === API_STATES.Loading && <div className="loader-container">Loading..</div>}
            {status === API_STATES.Error && (
                <div className="error">
                    <h1>{IsError}</h1>
                </div>
            )}
            {status === API_STATES.Loaded && DataofProducts.length === 0 && (
                <div className="error">
                    <h1>No products available at the moment. Please try again later.</h1>
                </div>
            )}
            {/* {status === API_STATES.Loaded && (
                <div className='All-data-container'>
                    <img className='img-container' src={DataofProducts.image} alt="" />
                    </div>
                    
            )} */}
            {status === API_STATES.Loaded && (
                <>
                    <div className=' image-div'>
                        <img className='img-container' src={(DataofProducts.image)} alt="" />
                    </div>
                    <div className='text-details'>
                        <div className='category'><b>Category : &nbsp;</b><p>{(DataofProducts.category
                        )}</p></div>
                        <div>
                            <span><b>Title:&nbsp;</b>{(DataofProducts.title)}</span>
                        </div>
                        <div>
                            <span><b>Price:</b> &nbsp;{(DataofProducts.price)}</span>
                        </div>
                        <div>

                            <span><b>Count: &nbsp;</b>{(DataofProducts.rating.count)}&nbsp; <b>Rate: &nbsp;</b> {(DataofProducts.rating.rate)}</span>
                        </div>
                        <span className='description'><b>Description:</b> &nbsp;{(DataofProducts.description)}</span>

                    </div>
                </>)}
        </>
    )

}


export default EachProduct
