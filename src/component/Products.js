import React from 'react'
import './Products.css'
import { Link } from 'react-router-dom'
function Products(props) {
    const {  id,image, category, title, price, description } = props.data
    return (
        <>
            
                <div className='check'>
                <Link to={`/EachProduct/${id}`}>
                    <div className=' image-div'>
                        <img className='img-container' src={(image)} alt="" />
                    </div>
                    <div className='text-details'>
                        <div className='category'><b>Category : &nbsp;</b><p>{(category
                        )}</p></div>
                        <div>
                            <span><b>Title:&nbsp;</b>{(title)}</span>
                        </div>
                        <div>
                            <span><b>Price:</b> &nbsp;{(price)}</span>
                        </div>
                        <div>
                            <span><b>Count: &nbsp;</b>{(props.data.rating.count)}&nbsp; <b>Rate: &nbsp;</b> {(props.data.rating.rate)}</span>
                        </div>
                        <span className='description'><b>Description:</b> &nbsp;{(description)}</span>

                    </div>
                    </Link>
                </div >
          


        </>

    )
}

export default Products
