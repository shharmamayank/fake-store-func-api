import React, { useEffect, useState } from 'react'
import "./AllProducts.css"
import Products from './Products'

function AllProducts() {
    const API_STATES = {
        Loading: "Loading",
        Loaded: "Loaded",
        Error: "error"
    }
    const [post, SetPost] = useState([])
    const [status, SetStatus] = useState(API_STATES)
    const [IsError, setIsError] = useState("");


    const url = 'https://fakestoreapi.com/products'

    const fetchData = ((url) => {
        SetStatus(API_STATES.Loading)

        fetch(url).then((response) => {
            return response.json()
        }).then((response) => {
            SetPost(response)
            SetStatus(API_STATES.Loaded)
        }).catch((error) => {
            setIsError(
                "An API error occurred while fetching"
            );
            SetStatus(API_STATES.Error);

        })

    })

    useEffect(() => {
        fetchData(url)
    }, [])

    let DataofProducts = post

    const fetchedProductData = DataofProducts.map(data => {

        return (<Products key={data.id} data={data} />)
    })
    return (
        <>
            {status === API_STATES.Loading && <div className="loader-container">Loading..</div>}
            {status === API_STATES.Error && (
                <div className="error">
                    <h1>{IsError}</h1>
                </div>
            )}
            {status === API_STATES.Loaded && DataofProducts.length === 0 && (
                <div className="error">
                    <h1>No products available at the moment. Please try again later.</h1>


                </div>
            )}
            {status === API_STATES.Loaded && (
                <div className='All-data-container'>{fetchedProductData}</div>
            )}
        </>
    )
}

export default AllProducts
